/**
 * Dit project kan je de temperaturen converten.
 */
function convertTemperature(temperatureScaleOriginal, temperature) {
	let temperatureScale = temperatureScaleOriginal;
	let temperatureOriginial = temperature;

	let newTemperature;

	if (temperatureScale === "Celcius") {

		newTemperature = temperatureOriginial * 9 / 5 + 32;
		temperatureScale = "Fahrenheit";
		console
				.log(`Nieuwe temperatuur is ${newTemperature} ${temperatureScale}`);

	} else if (temperatureScale === "Fahrenheit") {
		newTemperature = (temperatureOriginial - 32) * 5 / 9;
		temperatureScale = "Celcius";
		console
				.log(`Nieuwe temperatuur is ${newTemperature} ${temperatureScale}`);
	} else {
		console.log("Verkeerde input! Controleer de naam die ingegeven is.")
	}

}

console.log(convertTemperature("Celcius", 32));
console.log(convertTemperature("Fahrenheit", 50));
console.log(convertTemperature("nope", 30));